TJV semestral work

client - https://github.com/chladas0/BI-TJV-client
backend - https://github.com/chladas0/BI-TJV-backend


# IssueTracker

Welcome to IssueTracker, a project management tool for tracking projects, tasks, and managing unfinished tasks.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)

## Introduction

IssueTracker is a web-based application built with React and Material-UI for the frontend, and Spring Boot for the backend. It is designed to help users manage projects and tasks efficiently. This project includes features such as creating, updating, deleting projects or tasks and a dashboard to display unfinished tasks. 

## Features
- Register/Login
- View a list of projects with options to edit, delete, and view project details.
- Create new projects and update existing ones.
- Delete projects with a confirmation dialog to prevent accidental deletion.
- Access a dashboard displaying unfinished tasks, with the ability to click and view task details.
